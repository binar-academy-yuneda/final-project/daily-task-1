const stringContain = (number, find) => {
  const numberString = number.toString();
  for (const iterator of numberString) {
    if (iterator == find) {
      return true;
    }
  }
};

const sumNumber = (number) => {
  const numberString = number.toString();
  let sum = 0;
  for (const iterator of numberString) {
    sum += Number(iterator);
  }
  return sum;
};

const printValue = (number, value) => {
  if (
    number % value == 0 ||
    stringContain(number, value) ||
    sumNumber(number) % value == 0
  ) {
    return true;
  }
  return false;
};

for (let index = 1; index <= 100; index++) {
  if (printValue(index, 4) && printValue(index, 8)) {
    console.log(`${index} -> BullDog`);
  } else if (printValue(index, 4)) {
    console.log(`${index} -> Bull`);
  } else if (printValue(index, 8)) {
    console.log(`${index} -> Dog`);
  } else {
    console.log(`${index} -> ${index}`);
  }
}
