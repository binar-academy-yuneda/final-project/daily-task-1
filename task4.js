// Algorithm
// 1. find remainder with modulus 6 (numbe % 6)
// 2. use modulus 6 again for remainder from previous iteration
// 3. The iteration runs until the remainder result in 0
// 4. the result is the remainder starts from last remainder before 0 to first remainder
//
// Example ("25" base 10 is equal to what in base 6 )
// Iteration 0 => 25 = 6x4 + 1
// Iteration 1 => 1  = 6x0 + 4
// Iteration 2 => 0  = 6x0 + 0
//

const convertDecimalToAnyBase = (number, base) => {
  let result = [];
  let remainder;
  let n = number;
  while (n !== 0) {
    remainder = n % base;
    n = (n - remainder) / base;
    result.unshift(remainder);
  }
  return result.join('');
};

// convert 25 in base 10 to number in base 6
console.log(convertDecimalToAnyBase(25, 6));
